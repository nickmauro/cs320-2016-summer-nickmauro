(* ****** ****** *)
//
#include
"./../assign4.dats"
//
staload "./../../../mylib/mystream.dats"
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

fun
summa(sd: stream(double)): stream(double) = let
  fun helper(sd: stream(double), tot: double) =
    $delay(case+ !sd of
      | stream_nil() => stream_nil()
      | stream_cons(x, xs) => stream_cons(tot, helper(xs, x+tot)))
in
  helper(sd, 0.0)
end

fun
incone(i: int): stream(int) = $delay(stream_cons(i, incone(i+1)))

fun
negswitch(si: stream(int)): stream(int) = let
  fun helper(si: stream(int), posneg: int): stream(int) = 
    $delay(case+ !si of
      | stream_nil() => stream_nil()
      | stream_cons(x, xs) => stream_cons(x*posneg, helper(xs, ~posneg)))
in
  helper(si, 1)
end

implement
stream_ln2() = summa(stream_map_cloref<int><double>(v, lam(a) => 1.0/a)) where {val v = negswitch(incone(1))}

(* This code is based on /lectures/lecture-06-21/mystream_test.dats *)

implement
intpair_enumerate() = let
  val st0 = (0, 0)
  fun fopr(st: int2): $tup(int2, int2) = let
    val i = st.0
    val j = st.1
  in
    if i <= j then $tup((i, j), (i+1, j)) else fopr((0, j+1))
  end
in
  mystream_make_cloref<int2,int2>(st0, lam(int2) => fopr(int2))
end

(* This code is based on https://www.cs.bu.edu/~hwxi/academic/courses/CS320/Summer10/code/lab17/eulertrans.dats *)

implement
EulerTrans(xs) = $delay(stream_cons(v3-b*b/(a+b), EulerTrans(v11)) where {
  val- stream_cons(v1, v11) = !xs
  val- stream_cons(v2, v22) = !v11
  val- stream_cons(v3, v33) = !v22
  val a = v1 - v2
  val b = v3 - v2})

implement
main0 () =
{
//
val () =
println!
(
  "Hello from [assign9_sol]!"
) (* val *)
//
val ln2_1M =
stream_nth_exn (stream_ln2(), 1000000)
val () = println! ("ln2_1M = ", ln2_1M)
//
val xys =
  intpair_enumerate()
val xys_10 = stream_take_exn(xys, 10)
val xys_10 = list0_of_list_vt(xys_10)
//
val () = println! ("xys_10 = ", xys_10)
//
val ln2 = stream_ln2()
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val () = println! ("ln2_4_0 = ", stream_nth_exn(ln2, 0))
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign4_sol.dats] *)
