//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #5
// Due Tuesday, the 28th of June, 2016 at 11:59pm
//
*)
(* ****** ****** *)

(*
#
# Assign5: 20 points
#
Please use the code in game-of-24.dats to
give a running implementation of Game-of-24
#
There is little code for you to write.
#
The assignment primarily expects that you read
the given code and then use it to give a running
implementation of Game-of-24.
#
*)

(* ****** ****** *)
//
#include
"./../game-of-24.dats"
//
(* ****** ****** *)

extern
fun
process_node(node): void

(* ****** ****** *)

implement
process_node
  (x0) = let
//
val cs = x0
//
in
//
case+ cs of
| list0_sing(c0) => let
    val v0 = card_eval(c0)
  in
    if abs(v0 - 24) < EPSILON then println! ("Solution: ", c0)
  end // end of [list0_sing]
| _(* non-sing *) => ()
//
end // end of [process_node]

(* ****** ****** *)
//
extern
fun
dfirst_search(node): void
//
(* ****** ****** *)

staload
"./../../../mylib/mystack.dats"

(* ****** ****** *)

implement
dfirst_search(nx) = let
//
val
stk = stack_make_nil()
//
val () = stack_push(stk, nx)
//
fun
loop
(
// argless
) : void = let
//
val
opt = stack_pop_opt(stk)
//
in
  case+ opt of
  | None() => ()
  | Some(nx) => let
      val () = process_node(nx)
      val nxs = node_get_children(nx)
      val () = stack_push_list(stk, nxs)
    in
      loop()
    end // end of [Some]
end // end of [loop]
//
in
  loop()
end // end of [dfirst_search]

(* ****** ****** *)
//
implement
play_game_of_24(n1, n2, n3, n4) = let
  val v1 = CARDval(n1*1.0)
  val v2 = CARDval(n2*1.0)
  val v3 = CARDval(n3*1.0)
  val v4 = CARDval(n4*1.0)
  val v = g0ofg1($list{card}(v1,v2,v3,v4))
in
  dfirst_search(v)
end
  
(* ****** ****** *)

staload "libc/SATS/stdlib.sats"

(* ****** ****** *)

implement
main0(argc, argv) = let
//
val n1 = (if argc >= 2 then atoi(argv[1]) else 0): int
val n2 = (if argc >= 3 then atoi(argv[2]) else 0): int
val n3 = (if argc >= 4 then atoi(argv[3]) else 0): int
val n4 = (if argc >= 5 then atoi(argv[4]) else 0): int
//
in
  play_game_of_24(n1, n2, n3, n4)
end // end of [main0]

(* ****** ****** *)

(* end of [assign8_sol.dats] *)