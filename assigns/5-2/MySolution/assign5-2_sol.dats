//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #5-2
// Due Tuesday, the 28th of June, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include "./../doublet.dats"
//
(* ****** ****** *)

(*
**
** HX: 30 points
**
** Please read doublet.dats and
** then try to implement doublet2
**
*)

(* ****** ****** *)
//
// HX-2016-06-21:
// Please implement
// the function doublet2 here!
//
(* This code is based on https://bitbucket.org/bithwxi/cs320-2016-summer/src/b749baccfae2f96122eea2471e565da9ed895a39/assigns/5-2/doublet.dats?at=master&fileviewer=file-view-default *)

local
#include
"./../../../mylib/myqueue.dats"

typedef node2 = list0(node)

extern
fun
node_get_word2(node2): word
implement
node_get_word2(n2) = node_get_word(n2.head())

extern
fun
node_mark2(node2): void
implement
node_mark2(n2) = node_mark(n2.head())

extern
fun
node_marked2(node2): bool
implement
node_marked2(n2) = node_marked(n2.head())

extern
fun
node_get_children2(node2): list0(node2)
implement
node_get_children2(n2) = let
  val n0 = node_get_children(n2.head())
in
  list0_map(n0, lam(a) => cons0(a, n2))
end

extern
fun{}
process2(node2): void

extern
fun{}
bfirst_search2(node2): void

implement{}
bfirst_search2(n2) = let
  val stk = queue_make_nil()
  val () = queue_enqueue(stk, n2)
fun loop() = let 
  val opt = queue_dequeue_opt(stk)
in
  case+ opt of
  | None() => ()
  | Some(n2) => let
      val () = process2(n2)
      val nxs = node_get_children2(n2)
      val nxs = list0_filter(nxs, lam(n2) => ~node_marked2(n2))
      val () = queue_enqueue_list(stk, nxs)
    in 
      loop()
    end
end
in
  loop()
end 

in

implement
doublet2(w1, w2) = let
  exception Found of (node2)
  exception NONWORDEXN of ()
implement
process2<>(n2) = let
  val () = node_mark2(n2)
in
  if node_get_word2(n2) = w2 then $raise(Found(n2)) else ()
end
  val () = dict_unmarking()
  val opt = word_get_node(w1)
  val nx2 = (case+ opt of
    | Some(n2) => cons0(n2, nil0())
    | None() => $raise NONWORDEXN()): node2
in
  try
  let
    val () = bfirst_search2(nx2) in None()
end
  with ~Found(n2) => Some(n2)
end
end

(* ****** ****** *)
//
staload
"libats/libc/SATS/stdlib.sats"
//
(* ****** ****** *)

implement
main0(argc, argv) = let
//
val w1 =
(
  if argc >= 2 then argv[1] else ""
) : string // end-of-val
val w2 =
(
  if argc >= 3 then argv[2] else ""
) : string // end-of-val
//
val opt = doublet2(w1, w2)
//
in
//
case+ opt of
| None() =>
  println!(w1, '/', w2, " are not a doublet!")
| Some(nxs) =>
  println!(w1, '/', w2, " are a doublet: ", nxs)
//
end // end of [main0]

(* ****** ****** *)

(* end of [assign5-2_sol.dats] *)
