(*
** Refinement-based Programming
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
(* ****** ****** *)

macdef
prelude_string_tabulate = string_tabulate

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
staload "./../../mylib/myfile.dats"
//
(* ****** ****** *)
//
typedef word = string
//
datatype
node = NODE of (word, ref(int))
//
extern
fun
node_get_word(node): word
//
extern
fun
node_make_word(word): node
//
implement
node_get_word(nx) =
  let val+NODE(w0, _) = nx in w0 end
//
implement
node_make_word(w0) = NODE(w0, ref(0))
//
(* ****** ****** *)
//
extern
fun
print_node : node -> void
extern
fun
fprint_node : fprint_type(node)
//
overload print with print_node
overload fprint with fprint_node
//
implement
print_node(nx) =
  fprint_node(stdout_ref, nx)
//
implement
fprint_node
  (out, nx) =
let
  val+NODE(w0, _) = nx in fprint(out, w0)
end // end of [fprint_node]
//
(* ****** ****** *)

implement fprint_val<node> = fprint_node

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
(* ****** ****** *)

staload HT =
{
//
#define CAPACITY 1024
//
typedef
key = string and itm = ptr
//
#include
"share/atspre_define.hats"
//
#include
"{$LIBATSHWXI}/globals/HATS/ghashtbl_linprb.hats"
//
} (* end of [staload] *)

(* ****** ****** *)

#define WORDS "./dict_words.txt"

(* ****** ****** *)
//
extern
fun
word_get_node(w0: word): Option(node)
//
(* ****** ****** *)

local

(*
fun
dict_initize(): void =
{
//
(*
val () =
println! ("$HT.get_size() = ", $HT.get_size())
val () =
println! ("$HT.get_capacity() = ", $HT.get_capacity())
*)
//
val-~Some_vt(filr) =
  fileref_open_opt (WORDS, file_mode_r)
//
val () = let
//
fun loop
(
  filr: FILEref
) : void = let
  val isnot = fileref_isnot_eof (filr)
in
  if isnot
    then let
      val str =
        fileref_get_line_string (filr)
      // end of [val]
      val-~None_vt() =
        $HT.insert_opt (str, the_null_ptr)
      // end of [val]
    in
      loop (filr)
    end // end of [then]
    else () // end of [else]
  // end of [if]
end // end of [loop]
//
in
  loop (filr)
end // end of [val]
//
val ((*void*)) = fileref_close (filr)
//
val () =
println! ("$HT.get_size() = ", $HT.get_size())
val () =
println! ("$HT.get_capacity() = ", $HT.get_capacity())
//
} (* end of [dict_initize] *)
*)

fun
dict_initize(): void =
{
//
(*
val () =
println! ("$HT.get_size() = ", $HT.get_size())
val () =
println! ("$HT.get_capacity() = ", $HT.get_capacity())
*)
//
val-~Some_vt(filr) =
  fileref_open_opt(WORDS, file_mode_r)
//
val theDict = fileref_streamize(filr)
//
val () = loop(theDict) where
{
  fun loop(ws: stream(string)): void =
    (
      case+ !ws of
      | stream_nil() => ()
      | stream_cons(w, ws) => let
          val-~None_vt() =
            $HT.insert_opt (w, the_null_ptr)
          // end of [val]
        in
          loop(ws)
        end // end of [stream_cons]
    )
}
//
val () =
println! ("$HT.get_size() = ", $HT.get_size())
val () =
println! ("$HT.get_capacity() = ", $HT.get_capacity())
//
} (* end of [dict_initize] *)

val ((*initize*)) = dict_initize()

in
//
implement
word_get_node(w0) = let
//
val p0 =
cptr2ptr
(
  $HT.search_ref(w0)
) (* val *)
//
in
//
if
(p0 > 0)
then let
  val nx =
    $UN.ptr0_get<ptr>(p0)
  // end of [val]
in
//
if
nx > 0
then Some($UN.cast{node}(nx))
else let
  val nx = node_make_word(w0)
  val () = $UN.ptr0_set<node>(p0, nx)
//
in
  Some(nx)
end // end of [else]
//
end // end of [then]
else None() // end of [else]
//
end // end of [word_get_node]
//
end // end of [local]

(* ****** ****** *)
//
extern
fun
node_mark(node): void
implement
node_mark(nx) =
  let val NODE(_, r) = nx in !r := !r + 1 end
//
extern
fun
node_unmark(node): void
implement
node_unmark(nx) = let val NODE(_, r) = nx in !r := 0 end
//
(* ****** ****** *)
//
extern
fun
dict_unmarking(): void
//
(*
implement
dict_unmarking() = let
//
typedef
kx = (string, ptr)
//
val kxs = $HT.listize1()
//
fun
loop
(
  kxs: List0_vt(kx)
) : void =
(
case+ kxs of
| ~list_vt_nil() => ()
| ~list_vt_cons(kx, kxs) => let
    val p = kx.1
    val () =
    if p > 0 then
      node_unmark($UN.cast{node}(p))
    // end of [if]
  in
    loop(kxs)
  end // end of [list_vt_cons]
) (* end of [loop] *)
//
in
  loop(kxs)
end // end of [dict_unmarking]
*)
//
implement
dict_unmarking() =
$HT.foreach_cloref
(
lam(k, x) =>
  if (x) > 0 then node_unmark($UN.cast{node}(x))
// end of [lam]
) (* end of [dict_unmarking] *)
//
(* ****** ****** *)

exception WORDSUBEXN of ()

(* ****** ****** *)

extern
fun
word_get_at(word, int): char

implement
word_get_at(w0, i0) = let
//
val i0 = g1ofg0(i0)
val w0 = g1ofg0(w0)
val n0 = sz2i(length(w0))
//
in
//
if (i0 < 0)
  then $raise WORDSUBEXN()
  else (
    if (i0 >= n0)
      then $raise WORDSUBEXN() else w0[i0]
  ) (* end of [else] *)
//
end // end of [word_get_at]

(* ****** ****** *)
//
extern
fun
word_set_at
  (word, int, char): word
//
implement
word_set_at(w0, i0, c0) = let
//
val i0 = g1ofg0(i0)
val w0 = g1ofg0(w0)
val n0 = sz2i(length(w0))
//
val c0 = $UN.cast{charNZ}(c0)
//
in
//
if (i0 < 0)
then $raise WORDSUBEXN()
else if (i0 >= n0)
  then $raise WORDSUBEXN()
  else let
    val i0 = i2sz(i0)
    val n0 = i2sz(n0)
  in
    $UN.castvwtp0(string_tabulate_cloref(n0, lam(i) => if i = i0 then c0 else w0[i]))
  end // end of [else]
//
end // end of [word_set_at]
//
(* ****** ****** *)
//
extern
fun
word_change_at
  (word, int): list0(node)
//
implement
word_change_at
  (w0, i) = let
//
val c0 =
word_get_at(w0, i)
//
fun
aux
(
  k: int
, res: list0(node)
) : list0(node) =
(
if
(k < 26)
then let
  val c = 'a' + k
in
  if c = c0
    then aux(k+1, res)
    else let
      val w1 =
        word_set_at(w0, i, c)
      // end of [val]
      val opt = word_get_node(w1)
    in
      case+ opt of
      | None() => aux(k+1, res)
      | Some(nx) => aux(k+1, cons0(nx, res))
    end
end // end of [then]
else list0_reverse(res)
)
//
in
  aux(0, nil0())
end // end of [word_change_at]
//
(* ****** ****** *)
//
extern
fun
word_change_all
  (word): list0(node)
//
implement
word_change_all
  (w0) = let
//
val n0 = length(w0)
//
val nxs = list0_tabulate(sz2i(n0), lam(i) => word_change_at(w0, i))
//
in
  list0_concat(nxs)
end // end of [word_change_all]
//
(* ****** ****** *)
//
extern
fun
node_marked(node): bool
implement
node_marked(nx) = let val NODE(_, r) = nx in !r > 0 end
//
(* ****** ****** *)

extern
fun
node_get_children(node): list0(node)

(* ****** ****** *)

implement
node_get_children
  (nx) = let
//
val w0 =
  node_get_word(nx)
//
val n0 = sz2i(length(w0))
//
fun
aux
(
  i: int
) : list0(list0(node)) =
  if i < n0 then cons0(word_change_at(w0, i), aux(i+1)) else nil0()
//
in
  list0_concat(aux(0))
end // end of [node_get_children]

(* ****** ****** *)
//
extern
fun
doublet(w1: word, w2: word): bool = "mac#"
//
extern
fun
doublet2(w1: word, w2: word): Option(list0(node)) = "mac#"
//
(* ****** ****** *)

#ifndef
ASSIGN5_2_SOL
#then
local
//
#include
"./../../mylib/myqueue.dats"
//
extern
fun{}
process(node): void
extern
fun{}
bfirst_search(node): void
//
(* ****** ****** *)

implement
{}(*tmp*)
bfirst_search(nx) = let
//
val
stk = queue_make_nil()
//
val () = queue_enqueue(stk, nx)
//
fun loop() = let
  val opt = queue_dequeue_opt(stk)
in
  case+ opt of
  | None() => ()
  | Some(nx) => let
      val () = process(nx)
      val nxs =
        node_get_children(nx)
      val nxs =
      list0_filter
        (nxs, lam(nx) => ~node_marked(nx))
      // end of [val]
      val ((*void*)) =
        queue_enqueue_list(stk, nxs)
      val ((*void*)) =
        list0_foreach(nxs, lam(nx) => node_mark(nx))
    in
      loop()
    end // end of [Some]
end // end of [bfirst_search]
//
in
  loop()
end // end of [bfirst_search]

in (* in-of-local *)

implement
doublet
  (w1, w2) = let
//
exception Found of (node)
//
exception NONWORDEXN of ()
//
implement
process<>(nx) =
(
//
if node_get_word(nx)=w2
  then $raise(Found(nx)) else ()
//
) (* end of [process] *)
//
val () = dict_unmarking()
//
val opt = word_get_node(w1)
//
val nx1 =
(
  case+ opt of
  | Some(nx) => nx | None() => $raise NONWORDEXN()
) : node // end-of-val
in
//
try
let
val () =
  bfirst_search(nx1) in false
end
with ~Found(nx) => true
//
end // end of [doublet]

end // end of [local]
#endif // end of [#ifndef]

(* ****** ****** *)

#ifdef
TEST_DOUBLET
#then
implement
main0() = {
//
val w1 = "clean"
and w2 = "dirty"
//
val ans = doublet(w1, w2)
//
val ((*void*)) =
(
case+ ans of
| true =>
  println!(w1, '/', w2, " are a doublet!")
| false =>
  println!(w1, '/', w2, " are not a doublet!")
)
//
val w1 = "wheat"
and w2 = "bread"
//
val ans = doublet(w1, w2)
//
val ((*void*)) =
(
case+ ans of
| true =>
  println!(w1, '/', w2, " are a doublet!")
| false =>
  println!(w1, '/', w2, " are not a doublet!")
)
//
val w1 = "water"
and w2 = "blood"
//
val ans = doublet(w1, w2)
//
val ((*void*)) =
(
case+ ans of
| true =>
  println!(w1, '/', w2, " are a doublet!")
| false =>
  println!(w1, '/', w2, " are not a doublet!")
)
//
} (* end of [main0] *)
#endif

(* ****** ****** *)

(* end of [doublet.dats] *)
