//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #3-3
// Due: Friday, June 10, 2016 at 11:59pm
//
*)
(* ****** ****** *)

#define
ATS_PACKNAME "CS320_SUMMER16_HW3_3"

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef intrep = list0(int)

(* ****** ****** *)

(*
A non-negative integer is represented as a list of
digits. For instance
//
0 -> () // emptylst
12345 -> (5, 4, 3, 2, 1)
//
*)

(* ****** ****** *)
//
// HX: 10 points
//    
extern
fun
intrep_add (intrep, intrep): intrep
//
(* ****** ****** *)
//
// HX: 10 points
//    
(*
//
// Please use [intrep_add] to solve
// the following problem:
//
// https://projecteuler.net/problem=16
//
*)
extern
fun pow2digitsum (n: int): int
//
(* ****** ****** *)
//
// HX: 20 points
//    
extern
fun
intrep_mul (intrep, intrep): intrep
//
(* ****** ****** *)

(* end of [assign04.dats] *)
