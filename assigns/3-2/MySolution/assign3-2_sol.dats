(* ****** ****** *)
//
#include
"./../assign3-2.dats"
staload 
"./../../../mylib/mylist.dats"
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_contain
  (xs, x0, eq) = (
  case+ xs of
  | list0_nil() => false
  | list0_cons(x, xs) => eq(x0, x) || mylist0_contain(xs, x0, eq)
)
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_find (xs, pred) = let
fun helper(loc: int, xs: list0(a)): int = (
  case+ xs of
  | list0_nil() => ~1
  | list0_cons(x, xs) => if pred(x) then loc else helper(loc+1, xs)
  )
in
  helper(0, xs)
end

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_rfind(xs, pred) = let
exception found of (int)
fun helper(x: int): int = ~1
in
try
helper(
mylist_foldleft_cloref<a,int>
(xs, 0, lam(res, x) => if pred(x) then $raise found(res) else helper(res+1))
)
with
| ~found(res) => res
end
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () =
{
//
val xs = list0_make_intrange (0, 10)
val () = assertloc (mylist0_contain<int> (xs, 5, lam (x1, x2) => x1 = x2))
val () = assertloc (~mylist0_contain<int> (xs, 10, lam (x1, x2) => x1 = x2))
//
val () = assertloc (mylist0_find<int> (xs, lam (x) => x > 4) = 5)
//val () = assertloc (mylist0_rfind<int> (xs, lam (x) => x mod 3 = 1) = 7)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3-2_sol.dats] *)