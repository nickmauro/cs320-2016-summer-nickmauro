(* ****** ****** *)

#include
"./../DigitalClock.dats"

(* ****** ****** *)
//
// HX: 30 points
// Please replace
// the following dummy implementations:
//
implement
draw_1(X, Y, W, H) = {
  val () = draw_vline(X+W-2, Y, H)
}

implement
draw_2(X, Y, W, H) =
{
  val () = draw_vline(X+1, Y+5, H)
  val () = draw_vline(X+W-2, Y-5, H)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

implement
draw_3(X, Y, W, H) =
{
  val () = draw_vline(X+W-2, Y, H)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

implement
draw_4(X, Y, W, H) = 
{
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_vline(X+W-2, Y, H)
  val () = draw_vline(X+1, Y, H-5)
}

implement
draw_5(X, Y, W, H) = 
{
  val () = draw_vline(X+W-2, Y+5, H)
  val () = draw_vline(X+1, Y, H-5)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

implement
draw_6(X, Y, W, H) = 
{
  val () = draw_vline(X+W-2, Y+5, H)
  val () = draw_vline(X+1, Y, H)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

implement
draw_7(X, Y, W, H) = 
{
  val () = draw_vline(X+W-2, Y, H)
  val () = draw_hline(X+1, Y, W-2)
}

implement
draw_8(X, Y, W, H) = 
{
  val () = draw_vline(X+W-2, Y, H)
  val () = draw_vline(X+1, Y, H)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

implement
draw_9(X, Y, W, H) = 
{
  val () = draw_vline(X+W-2, Y, H)
  val () = draw_vline(X+1, Y-5, H)
  val () = draw_hline(X+1, Y, W-2)
  val () = draw_hline(X+1, (Y+H)/2, W-2)
  val () = draw_hline(X+1, Y+H-1, W-2)
}

//
(* ****** ****** *)

(* end of [DigitalClock_sol.dats] *)
