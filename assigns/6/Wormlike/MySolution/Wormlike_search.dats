(* ****** ****** *)

fun
wnode_mark
(
  wx: wnode
) : void = let
//
val+WN(i, j) = wx
val V0 = theVisitbd_get() in V0[i,j] := 1
end // end of [wnode_mark]
//
fun
wnode_marked
(
  wx: wnode
) : bool = let
//
val+WN(i, j) = wx
val V0 = theVisitbd_get() in V0[i,j] >= 1
end // end of [wnode_marked]

(* ****** ****** *)
//
extern
fun
wnode_get_children : wnode -> list0(wnode)
//
implement
wnode_get_children
  (wx) = wxs where
{
//
val WN(i0, j0) = wx
//
val G0 = theGamebd_get()
//
fun test(xn: xnode): bool =
  case+ xn of
  | XN0() => true | XN1(knd) => knd >= 1
//
val
wxs = list0_nil()
//
val i1 = pred_row(i0) and j1 = j0
val wxs =
(
  if test(G0[i1, j1])
    then list0_cons(WN(i1, j1), wxs) else wxs
  // end of [if]
) : list0(wnode)
val i1 = succ_row(i0) and j1 = j0
val wxs =
(
  if test(G0[i1, j1])
    then list0_cons(WN(i1, j1), wxs) else wxs
  // end of [if]
) : list0(wnode)
//
val i1 = i0 and j1 = pred_col(j0)
val wxs =
(
  if test(G0[i1, j1])
    then list0_cons(WN(i1, j1), wxs) else wxs
  // end of [if]
) : list0(wnode)
val i1 = i0 and j1 = succ_col(j0)
val wxs =
(
  if test(G0[i1, j1])
    then list0_cons(WN(i1, j1), wxs) else wxs
  // end of [if]
) : list0(wnode)
//
} (* end of [wnode_get_children] *)
//
(* ****** ****** *)
//
staload"./mylist.dats"
staload"./myqueue.dats"
//
typedef wnode2 = list0(wnode)
//
//
// HX: please implement:
//
extern
fun
wnode2_mark : wnode2 -> void
and
wnode2_marked : wnode2 -> bool

implement 
wnode2_mark(w2) = let
  val-cons0(x, _) = w2 
in 
  wnode_mark(x) 
end
  
implement
wnode2_marked(w2) = let 
  val-cons0(x, _) = w2
in 
  wnode_marked(x) 
end
//
(* ****** ****** *)
//
// HX: please implement:
//
extern
fun
wnode2_get_children : wnode2 -> list0(wnode2)

implement
wnode2_get_children(w2) = let
  val-cons0(x, _) = w2
  val w = wnode_get_children(x)
in 
  list0_map_cloref(w, lam(x) => cons0(x, w2))
end
//
(* ****** ****** *)
//
extern
fun{}
process2(wnode2): bool
extern
fun{}
bfirst_search2(wnode2): Option(wnode2)
//
(* ****** ****** *)
//
implement
{}(*tmp*)
process2(w2x) = let
  val-list0_cons(x,_) = w2x
  val+WN(i, j) = x
  val G0 = theGamebd_get()
in
  case+ G0[i, j] of 
  | XN0() => false 
  | XN1(i) => i >= 1
end
//
(* ****** ****** *)
//
implement
{}(*tmp*)
bfirst_search2(w2x) = let
  val stk = queue_make_nil()
  val () = queue_enqueue(stk, w2x)
  fun helper(): Option(wnode2) = let
    val q = queue_dequeue_opt(stk)
  in
    case+ q of
    | None() => None()
    | Some(w2x) => let
      val p = process2(w2x)
    in 
      if p then Some(w2x)
    else let
      val w = wnode2_get_children(w2x)
      val w = list0_filter_cloref(w, lam(w2x) => ~wnode2_marked(w2x))
      val () = list0_foreach_cloref(w, lam(w2x) => wnode2_mark(w2x))
      val () = list0_foreach_cloref(w, lam(w2x) => queue_enqueue(stk,w2x))
    in
      helper()
    end
  end
  end
in
  helper()
end
//
(* ****** ****** *)

implement
theWorm_move_search
  ((*void*)) = let
//
val () =
  theVisitbd_reset()
//
val wxs = theWorm_get_list()
//
val opt =
(
case+ wxs of
| list0_nil() =>
    Some(WN(0, 0))
| list0_cons(wx, _) => let
    val opt = bfirst_search2(cons0(wx, nil0()))
  in
    case+ opt of
    | None() => None() where
      {
(*
        val () = alert("theWorm_move_search: None")
*)
      }
    | Some(wxs) => opt where
      {
//
        val n = list0_length(wxs)
//
        val () = assertloc(n >= 2)
(*
        val () = alert("theWorm_move_search: Some")
        val () = alert("theWorm_move_search: length = " + String(n))
*)
        val opt = list0_get_at_opt(wxs, n-2)
//
      } (* end of [Some] *)
  end // end of [list0_cons]
) : Option(wnode)
//
in
//
  case+ opt of
  | None _ => theWorm_move_random() | Some _ => theWorm_move_with(opt)
//
end // end of [theWorm_move_search]

(* ****** ****** *)

(* end of [Wormlist_search.dats] *)
