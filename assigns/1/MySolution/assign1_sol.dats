(* ****** ****** *)

#include "./../assign1.dats"

(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//

extern
fun fib_trec_loop : (int, int, int) -> int

implement
fib_trec(n) = fib_trec_loop(n, 1, 0)

implement
fib_trec_loop(n, va, pre) =
  if n = 0 then pre 
    else if n = 1 then va 
        else fib_trec_loop(n-1, va + pre, va)
  
//
(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//

implement
try_fact() = let
  fun helper(i: int): int = if fact(i) != 0 then helper(i+1) else i 
in
    helper(0)
end

//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val r1 = fib_trec(10)
val () = assertloc(fib(10) = r1)
val () = println! ("fib_trec(10) = ", r1)
//
val r2 = fib_trec(20)
val () = println! ("fib_trec(20) = ", r2)
val () = assertloc(fib(20) = r2)
//
val () = println! ("try_fact() = ", try_fact())
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign1_sol.dats] *)
