(* ****** ****** *)
//
#include
"./../assign3.dats"
#include
"../../../mylib/mylist.dats"
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_last (xs) = let
  fun helper(x: int, xs: list0(int)): int = (
    case+ xs of
      | list0_nil() => x
      | list0_cons(x,xs) => helper(x,xs)
      )
    in
  helper(0,xs)
    end
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_length (xs) = (
    case+ xs of
    | list0_nil () => 0
    | list0_cons (_, xs) => 1 + mylist0_length (xs) 
    )
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
mylist0_choose (xs, n) = (
  if mylist0_length(xs) > n then (
    case+ xs of
      | list0_nil() => list0_nil()
      | list0_cons(x, xs) => mylist_append(mylist_mcons(mylist0_choose(xs, n-1), x), mylist0_choose(xs, n))
      )
  else if mylist0_length(xs) < n then list0_nil()
  else list0_sing(xs)
)

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () = () where
{
//
val xs =
  list0_make_intrange(0, 10)
//
val () =
  assertloc (mylist0_last(xs) = list0_last_exn(xs))
//
val () =
  assertloc (mylist0_length(xs) = list0_length(xs))
//
val nxs = list0_length (xs)
val xss = mylist0_choose (xs, 2)
val ((*void*)) =
  assertloc (list0_length (xss) = nxs * (nxs-1) / 2)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3_sol.dats] *)