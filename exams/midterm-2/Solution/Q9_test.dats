(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload
"./../../../mylib/mylist.dats"

(* ****** ****** *)

#include "./../midterm-2.dats"

(* ****** ****** *)
//
extern
fun
mymat_make_cloref
  : ((int, int) -<cloref1> double) -> mymat
//
(* ****** ****** *)
//
local
//
assume mymat =
  (int, int) -<cloref1> double
//
in
//
implement
mymat_get_at(M, i, j) = M(i, j)
//
implement
mymat_remove_row(M, i0) =
  lam(i, j) => M(if i < i0 then i else i+1, j)
implement
mymat_remove_col(M, j0) =
  lam(i, j) => M(i, if j < j0 then j else j+1)
//
implement
mymat_make_cloref(funclo) = funclo
//
end // end of [local]
//
(* ****** ****** *)
//
// HX: please put your code here:
//
// implement
// mymat_eval_det (M, n) = ...
//
(* ****** ****** *)

implement
main0 () = () where
{
//
#define N 8
//
val M0 =
lam (i: int, j: int)
  : double =<cloref1> g0i2f(1+min(i,j))
//
val M0 = mymat_make_cloref(M0)
//
val () = println! ("det(M0) = ", mymat_eval_det (M0, N)) // = 1.0
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Q9_test.dats] *)
