(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

#include "./../midterm-2.dats"

(* ****** ****** *)

fun
summa(sd: stream(double)): stream(double) = let
  fun helper(sd: stream(double), tot: double): stream(double) =
    $delay(case+ !sd of
      | stream_nil() => stream_nil()
      | stream_cons(x, xs) => stream_cons(tot, helper(xs, x*tot)))
in
  helper(sd, 1.0)
end

fun
incone(d: double): stream(double) = $delay(stream_cons(d, incone(d+1)))

implement
Wallis() = summa(stream_map_cloref<double><double>(incone(1.0), lam(a) => ((2.0*a)/(2.0*a-1.0))*((2.0*a)/(2.0*a+1.0)))) 

implement 
main0() = () where {
    val xs = stream_nth_exn(Wallis(), 1000000)
    val () = println!(xs)
}