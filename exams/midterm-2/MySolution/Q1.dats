(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload
"./../../../mylib/mylist.dats"

(* ****** ****** *)

#include "./../midterm-2.dats"

(* ****** ****** *)

implement
mylist_duprem_l(xs) = let
    fun duprem(i: int, li: list0(int)): list0(int) =
        case+ li of
        | list0_nil() => list0_nil()
        | list0_cons(x, xs) => if i = x then duprem(i, xs) else list0_cons(x, duprem(i, xs))
in
    case+ xs of
    | list0_nil() => xs
    | list0_cons(x, xss) => list0_cons(x, mylist_duprem_l(duprem(x, xss)))
end

implement
mylist_duprem_r(xs) = let
    fun duprem(i: int, li: list0(int)): bool =
        case+ li of
        | list0_nil() => false
        | list0_cons(x, xs) => if i = x then true else duprem(i, xs)
in
    case+ xs of
    | list0_nil() => list0_nil()
    | list0_cons(x, xs) => (if duprem(x, v) then v else list0_cons(x, v)) where {val v = mylist_duprem_r(xs)}
end

implement
main0() = () where {
    val xs = g0ofg1($list{int}(1,2,3,4,3,2,1))
    val () = println!(mylist_duprem_l(xs))
    val () = println!(mylist_duprem_r(xs))
}