(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

#include "./../midterm-2.dats"

(* ****** ****** *)

implement
tree0_is_maxheap(t0) = let
    fun leftright(i: int, t1: tree0(int)): bool =
        case+ t1 of
        | tree0_nil() => true
        | tree0_cons(t1l, v, t1r) => if i >= v then leftright(v, t1l) && leftright(v, t1r) else false
in
    case+ t0 of
    | tree0_nil() => true
    | tree0_cons(t0l, root, t0r) => leftright(root, t0l) && leftright(root, t0r)
end

implement
main0() = () where {
    val truetree = tree0_cons(tree0_cons(tree0_nil(), 1, tree0_nil()), 2, tree0_cons(tree0_nil(), 1, tree0_nil()))
    val falsetree = tree0_cons(tree0_cons(tree0_nil(), 100, tree0_nil()), 5, tree0_cons(tree0_nil(), 4, tree0_nil()))
    val () = println!(tree0_is_maxheap(truetree))
    val () = println!(tree0_is_maxheap(falsetree))
}