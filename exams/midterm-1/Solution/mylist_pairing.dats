(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)
//
// HX:
// A dummy implementation
//
implement
{a}
mylist_pairing
  (xs) = let
//
val ys = list0_reverse(xs)
val zs = list0_take_exn(ys, length(xs)/2)
//
in
  mylist2_zip<a,a>(xs, zs)
end // end of [mylist_pairing]
//
(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,2,3,4,5,6))
//
val () = println! ("xs = ", xs)
//
val () = println! ("mylist_pairing(xs) = ")
val () =
mylist_foreach_cloref<$tup(int,int)>
  (mylist_pairing<int>(xs), lam(xx) => (print xx.0; print ' '; println! (xx.1)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_pairing.dats] *)
