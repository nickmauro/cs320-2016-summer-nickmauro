(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

implement
{a}(*tmp*)
mylist0_split
  (xs) =
(
  case+ xs of
  | list0_nil() => (nil0, nil0)
  | list0_cons(x, xs) => let
      val (ys, zs) = mylist0_split(xs) in (cons0(x, zs), ys)
    end // end of [list0_cons]
)

(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,2,3,4,5))
//
val (ys, zs) = mylist0_split<int>(xs)
val ((*void*)) = println! ("ys = ", ys)
val ((*void*)) = println! ("zs = ", zs)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_split.dats] *)
