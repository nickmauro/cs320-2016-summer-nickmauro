#include "./../midterm-1.dats"

implement
A_draw(n) = let
    fun one_char(c: char, i: int): void = (
        if i <= 0 then ()
        else let
            val _ = print c
          in
            one_char(c, i-1)
          end
        )
    fun one_line(i: int): void = let
        val _ = one_char(' ', i)
        val _ = one_char('*', i*2 + 1)
        in
        end
    fun theTop(height: int, space: int): void = (
        if height = n then () else let
            val _ = one_char(' ', 2*n - height)
            val _ = one_char('*', 1)
            val _ = one_char(' ', space)
            val _ = one_char('*', 1)
            val _ = println!()
      in
        theTop(height + 1, space + 2)
      end
      )
    fun theBottom(height: int): void = (
        if height > n then () else
        let
            val _ = one_char(' ', n - height)
            val _ = one_char('*', 1)
            val _ = one_char(' ', 2*(height + n) - 1)
            val _ = one_char('*', 1)
            val _ = println!()
        in 
            theBottom(height + 1)
        end
        )
    fun theTip(height: int): void = let
        val _ = one_char(' ', 2*n)
        val _ = one_char('*', 1)
        val _ = println!() 
    in 
    end
    val _ = theTip(n)
    val _ = theTop(1, 1)
    val _ = one_line(n)
    val _ = println!()
    val _ = theBottom(1)
    val _ = println!()
in
end
    
implement
main0() = () where {
    val () = A_draw(1)
    val () = A_draw(2)
    val () = A_draw(3)
    val () = A_draw(4)
    val () = A_draw(5)
}