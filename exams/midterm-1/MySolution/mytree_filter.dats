#include "./../midterm-1.dats"

implement{a}
mytree_filter(t, pred) = let
    fun helper(t: tree0(a)): list0(a) = (
    case+ t of
    | tree0_nil() => tree0_nil()
    | tree0_cons(tleft, _, tright) => if pred(tleft) then list0_cons(tleft, helper(tright)) else helper(tright)
    )
in
    helper(t)
end

implement
main0() = ()