#include "./../midterm-1.dats"

implement{a}
mytree_maxminHT(t0) = 
  case+ t0 of
  | tree0_nil => $tup(0, 0)
  | tree0_cons(tl, _, tr) => let 
            val $tup(tlmax, tlmin) = mytree_maxminHT(tl)
			val $tup(trmax, trmin) = mytree_maxminHT(tr)
			val max = lam(a: int, b: int): int => if a > b then a else b
			val min = lam(a: int, b: int): int => if a < b then a else b
in
    $tup(max(tlmax, trmax) + 1, min(tlmin, trmin) + 1)
end 

implement
main0() = ()