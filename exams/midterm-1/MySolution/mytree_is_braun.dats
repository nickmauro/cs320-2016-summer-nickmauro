#include "./../midterm-1.dats"

implement{a}
mytree_is_braun(xs) = let
  exception False of ()
  fun helper(ys: tree0(a)): int = (
      case+ ys of
    | tree0_nil () => 0
    | tree0_cons(tl, _, tr) => let
        val goleft = helper(tl) and goright = helper(tr)
        val tester = goleft - goright
      in
        if tester = 0 orelse tester = 1 then 
            (1 + goleft + goright) 
        else $raise False()
      end
      )
in
  try let
    val _ = helper(xs)
  in
    true
  end with
    ~False() => false 
end

implement
main0() = ()