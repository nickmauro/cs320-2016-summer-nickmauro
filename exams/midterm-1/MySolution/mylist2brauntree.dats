#include "./../midterm-1.dats"

implement{a} 
mylist2brauntree(xs) = let
fun helper(xs: list0(a), i: int): tree0(a) = (
  if i > 0 then branch(xs, list0_nil, i, i/2)
  else tree0_nil
)
and branch(xs: list0(a), ys: list0(a), p: int, q: int): tree0(a) = (
  if q > 0 then let
    val-list0_cons(x, xs) = xs
  in
    branch(xs, list0_cons{a}(x, ys), p, q-1)
  end 
  else let
    val-list0_cons(x, xs) = xs
  in
    tree0_cons{a}(helper(ys, p/2), x, helper(xs, p-1-p/2))
  end
)
in
  helper(xs, list0_length<a>(xs))
end

implement
main0() = ()