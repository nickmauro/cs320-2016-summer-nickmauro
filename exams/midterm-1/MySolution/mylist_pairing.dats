#include "./../midterm-1.dats"
#include "./../../../mylib/mylist.dats"

implement{a}
mylist_pairing(alist) = let
    val(alist, blist) = mylist0_split(alist)
    val(blist) = mylist_reverse(blist)
in
    mylist2_zip(alist, blist)
end

implement{a}
mylist0_split(xs) = let
     fun helper(xs: list0(a), flag: bool, run: (list0(a), list0(a))): (list0(a), list0(a)) = 
         case+ xs of
         | list0_nil() => run
         | list0_cons(x, xs) => 
             if flag then helper(xs, false, (list0_cons(x, run.0), run.1))
             else helper(xs, true, (run.0, list0_cons(x,run.1)))
in
    helper(xs, true, (list0_nil(), list0_nil()))
end

implement
main0() = ()