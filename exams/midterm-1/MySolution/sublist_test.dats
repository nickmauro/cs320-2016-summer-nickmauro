(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)
//
// HX:
// A dummy implementation
//
implement sublist_test(xs, ys) =
        if list0_is_empty(ys) then true
        else if list0_is_empty(xs) then false
        else
            if xs.head() = ys.head()
            then sublist_test(xs.tail(), ys.tail()) || sublist_test(xs.tail(), ys)
            else sublist_test(xs.tail(), ys)
//
(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,2,3,4,5))
//
val ys = g0ofg1($list{int}(1,3,5))
val zs = g0ofg1($list{int}(1,2,2))
//
val () = assertloc(sublist_test(xs, ys))
val () = assertloc(not(sublist_test(xs, zs)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [sublist_test.dats] *)
